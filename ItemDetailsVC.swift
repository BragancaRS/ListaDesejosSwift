//
//  ItemDetailsVC.swift
//  DreamList
//
//  Created by Raphael Bragança on 13/01/17.
//  Copyright © 2017 Raphael Bragança. All rights reserved.
//

import UIKit
import CoreData

class ItemDetailsVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var detailsField: UITextField!
    @IBOutlet weak var storeField: UITextField!
    
    @IBOutlet weak var tumbImage: UIImageView!
    
    var picker = UIPickerView()
    
    var storeArray = [Store]()
    var itemToEdit: Item?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        titleField.delegate = self
        priceField.delegate =  self
        detailsField.delegate = self
        
        
        
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
        
        

        getStores()
        
        if itemToEdit != nil {
            loadItemData()
        }
        
        
        picker.delegate = self
        picker.dataSource = self
        storeField.inputView = picker
        
    }
    
    
    // - UIPickerView - //
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return storeArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let store = storeArray[row]
        return store.name
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // init the Field
        let store = storeArray[row]
        self.storeField.text = store.name
    }
    // - UIPickerView - //
    
    
    func getStores() {
        
        let fetchRequest: NSFetchRequest<Store> = Store.fetchRequest()
        
        do {
            self.storeArray = try context.fetch(fetchRequest)
            self.picker.reloadAllComponents()
        } catch {
            
        }
    }
    
    
    
    func loadItemData() {
        
        if let item = itemToEdit {
            
            titleField.text = item.title
            priceField.text = "\(item.price)"
            detailsField.text = item.details
            tumbImage.image = item.toImage?.image as? UIImage
            
            if let store =  item.toStore {
                var index = 0
                repeat {
                    let s = storeArray[index]
                    if s.name == store.name {
                        storeField.text = s.name
                    }
                    index += 1
                    
                    
                } while (index < storeArray.count)
            }
            
        }
    }
    
    
    @IBAction func addImage(_ sender: UIButton) {
        //present(imagePicker, animated: true, completion: nil)
        
        let myPickerImage = UIImagePickerController()
        myPickerImage.delegate = self
        myPickerImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerImage.allowsEditing = true
        
        present(myPickerImage, animated: true, completion: nil)
        
        
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        tumbImage.image = info[UIImagePickerControllerEditedImage] as? UIImage
        dismiss(animated: true, completion: nil)
        
        
        
//        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            
//            tumbImage.image = image
//        }
//        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    @IBAction func deletePressed(_ sender: UIBarButtonItem) {
        if itemToEdit != nil {
            context.delete(itemToEdit!)
            ad.saveContext()
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    
    // - Function that maks Insert Method - //
    @IBAction func savePressed(_ sender: UIButton) {
        
        var item: Item!
        
        let picture = Image(context: context)
        picture.image = tumbImage.image    // image tumb
 
        
        if itemToEdit == nil {
            item = Item(context: context)
        } else {
            item = itemToEdit
        }
        
        
        item.toImage = picture
        
        
        
        // Securing Data for Save
        if let title = titleField.text {
            item.title = title
        }
        if let price = priceField.text {
            item.price  = (price as NSString).doubleValue // convert the price init String end finish Double
        }
        if let details = detailsField.text {
            item.details = details
        }
        
        if let store = storeField.text {
            item.toStore?.name = store 
        }
        
        ad.saveContext()
        _ = navigationController?.popViewController(animated: true) // return the main Page
    }
    // - Function that maks Insert Method - //
    
    
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.titleField.resignFirstResponder()
        self.priceField.resignFirstResponder()
        self.detailsField.resignFirstResponder()
    
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
